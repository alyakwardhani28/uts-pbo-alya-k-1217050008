## Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. 


```java
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class User {
    private String name;
    private String phoneNumber;

    public User(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    // getter and setter methods

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String toString() {
        return "User: " + name + ", Phone: " + phoneNumber;
    }
}

class Driver {
    private String name;
    private String vehicleNumber;
    private String vehicleType;
    private String gender;
    private String vehicleColor;

    public Driver(String name, String vehicleNumber, String vehicleType, String gender, String vehicleColor) {
        this.name = name;
        this.vehicleNumber = vehicleNumber;
        this.vehicleType = vehicleType;
        this.gender = gender;
        this.vehicleColor = vehicleColor;
    }

    public String getName() {
        return name;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getGender() {
        return gender;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    // getter and setter methods

    public String toString() {
        return "Driver: " + name + ", No Plat: " + vehicleNumber + ", Tipe Kendaraan: " + vehicleType + ", Jenis Kelamin: " + gender +
                ", Warna Kendaraan: " + vehicleColor;
    }
}

class Ride {
    private User user;
    private Driver driver;
    private String pickUpLocation;
    private String destination;
    private double distance;
    private double fare;

    public Ride(User user, Driver driver, String pickUpLocation, String destination, double distance) {
        this.user = user;
        this.driver = driver;
        this.pickUpLocation = pickUpLocation;
        this.destination = destination;
        this.distance = distance;
        this.fare = calculateFare();
    }

    private double calculateFare() {
        double perKmRate;
        if (driver.getVehicleType().equalsIgnoreCase("Motor")) {
            perKmRate = 2000; // Biaya per KM untuk motor
        } else {
            perKmRate = 5000; // Biaya per KM untuk mobil
        }
        return distance * perKmRate;
    }

    public double getFare() {
        return fare;
    }

    public String toString() {
        return "Detail Pengendara:\n" + user.toString() + "\n" + driver.toString() + "\nLokasi Penjemputan: " + pickUpLocation + ", Tujuan: " + destination +
                "\nJarak: " + distance + " KM" + "\nHarga: " + fare;
    }
}

class OjekOnlineApp {
    private List<User> userList;
    private List<Driver> driverList;
    private List<Ride> rideList;

    public OjekOnlineApp() {
        userList = new ArrayList<>();
        driverList = new ArrayList<>();
        rideList = new ArrayList<>();
    }

    public void addUser(User user) {
        userList.add(user);
    }

    public void addDriver(Driver driver) {
        driverList.add(driver);
    }

    public void requestRide(User user, String pickUpLocation, String destination, double distance, String vehicleType) {
        if (driverList.size() > 0) {
            List<Driver> availableDrivers = new ArrayList<>();
            for (Driver driver : driverList) {
                if (driver.getVehicleType().equalsIgnoreCase(vehicleType)) {
                    availableDrivers.add(driver);
                }
            }

            if (availableDrivers.size() > 0) {
                System.out.println("Pengendara Tersedia " + vehicleType + ":");
                for (int i = 0; i < availableDrivers.size(); i++) {
                    Driver driver = availableDrivers.get(i);
                    System.out.println((i + 1) + ". " + driver.getName() + ", No Plat: " + driver.getVehicleNumber());
                }

                System.out.print("Pilih Pengemudi (1-" + availableDrivers.size() + "): ");
                Scanner scanner = new Scanner(System.in);
                int driverChoice = scanner.nextInt();

                if (driverChoice >= 1 && driverChoice <= availableDrivers.size()) {
                    Driver selectedDriver = availableDrivers.get(driverChoice - 1);
                    Ride ride = new Ride(user, selectedDriver, pickUpLocation, destination, distance);
                    rideList.add(ride);
                    driverList.remove(selectedDriver);

                    System.out.println("Pengendara Dipesan: " + selectedDriver.getName() + ", Type Kendaraan: " + selectedDriver.getVehicleType());
                    System.out.println("Jenis Kelamin: " + selectedDriver.getGender());
                    System.out.println("Plat No: " + selectedDriver.getVehicleNumber());
                    System.out.println("Warna Kendaraan: " + selectedDriver.getVehicleColor());
                    System.out.println("Tarif: " + ride.getFare());

                    boolean paymentSuccessful = false;
                    while (!paymentSuccessful) {
                        System.out.println("\nOpsi Pembayaran:");
                        System.out.println("1. Bayar Via DANA");
                        System.out.println("2. Bayar Tunai");
                        System.out.print("Pilih Opsi Pembayaran(1/2): ");
                        int paymentMethod = scanner.nextInt();

                        switch (paymentMethod) {
                            case 1:
                                System.out.println("Bayar Via DANA");
                                System.out.print("Masukan Jumlah Tagihan: ");
                                double amountPaid = scanner.nextDouble();
                                if (amountPaid == ride.getFare()) {
                                    System.out.println("Pembayaran Berhasil. Nikmati Perjalanan!");
                                    paymentSuccessful = true;
                                } else if (amountPaid > ride.getFare()) {
                                    double change = amountPaid - ride.getFare();
                                    System.out.println("Pembayaran Berhasil. Nikmati Perjalanan! Kemablian Sebesar: " + change);
                                    paymentSuccessful = true;
                                } else {
                                    System.out.println("Inputan tidak Valid. Coba Lagi.");
                                }
                                break;
                            case 2:
                                System.out.println("Bayar Tunai");
                                System.out.print("Masukan Jumlah Tagihan: ");
                                double cashPaid = scanner.nextDouble();
                                if (cashPaid == ride.getFare()) {
                                    System.out.println("Pembayaran Berhasil. Nikmati Perjalanan!");
                                    paymentSuccessful = true;
                                } else if (cashPaid > ride.getFare()) {
                                    double change = cashPaid - ride.getFare();
                                    System.out.println("Pembayaran Berhasil. Nikmati Perjalanan! Kemablian Sebesar: " + change);
                                    paymentSuccessful = true;
                                } else {
                                    System.out.println("Inputan tidak Valid. Coba Lagi.");
                                }
                                break;
                            default:
                                System.out.println("P

embayaran Tidak Valid Pastikan Memilih 1 atau 2");
                        }
                    }
                } else {
                    System.out.println("Invalid memilih pengemudi. Coba Lagi.");
                }
            } else {
                System.out.println("Drivers tidak tersedia dengan kategori tertentu. Silahkan coba lagi.");
            }
        } else {
            System.out.println("Drivers sedang tidak tersedia. Mohon ciba lagi nanti.");
        }
    }

    public void printRideDetails() {
        for (Ride ride : rideList) {
            System.out.println(ride.toString());
            System.out.println("-------------------------");
        }
    }
}

public class Main {
    public static void main(String[] args) {
        OjekOnlineApp app = new OjekOnlineApp();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Registrasi ");
        System.out.print("Input Nama Anda: ");
        String userName = scanner.nextLine();

        System.out.print("Input No Telpon: ");
        String userPhoneNumber = scanner.nextLine();

        User user = new User(userName, userPhoneNumber);
        app.addUser(user);

        System.out.println("Registrasi Berhasil!");

        boolean loginSuccessful = false;
        while (!loginSuccessful) {
            System.out.println("\nLogin");
            System.out.print("Input Nama Anda: ");
            String loginName = scanner.nextLine();

            System.out.print("Input No Telpon: ");
            String loginPhoneNumber = scanner.nextLine();

            if (user.getName().equalsIgnoreCase(loginName) && user.getPhoneNumber().equalsIgnoreCase(loginPhoneNumber)) {
                System.out.println("Login Berhasil!");
                loginSuccessful = true;

                Driver driver1 = new Driver("Mike", "ABC123", "Motor", "Pria", "Merah");
                Driver driver2 = new Driver("Sarah", "XYZ789", "Car", "Wanita", "Biru");
                // ... tambahkan pengemudi lainnya
                app.addDriver(driver1);
                app.addDriver(driver2);
                // ... tambahkan pengemudi lainnya

                System.out.print("Masukan Lokasi Penjemputan: ");
                String pickUpLocation = scanner.nextLine();

                System.out.print("Tujuan: ");
                String destination = scanner.nextLine();

                System.out.print("Jarak (KM): ");
                double distance = scanner.nextDouble();

                System.out.print("Masukan Jenis Kendaraan (Motor/Car): ");
                scanner.nextLine(); // consume newline character
                String vehicleType = scanner.nextLine();

                app.requestRide(user, pickUpLocation, destination, distance, vehicleType);

                app.printRideDetails();
            } else {
                System.out.println("Invalid. Coba Lagi.");
            }
        }
    }
}
```

Dalam contoh di atas, pendekatan matematika dan algoritma pemrograman digunakan dalam beberapa aspek:

1. Perhitungan Tarif: Metode `calculateFare()` dalam kelas `Ride` digunakan untuk menghitung tarif perjalanan berdasarkan jarak dan tipe kendaraan. Biaya per kilometer ditentukan berdasarkan jenis kendaraan, dan tarif dihitung dengan mengalikan jarak dengan biaya per kilometer.

2. Seleksi Pengemudi: Metode `requestRide()` dalam kelas `OjekOnlineApp` digunakan untuk memilih pengemudi yang tersedia berdasarkan jenis kendaraan yang diminta oleh pengguna. Pengemudi dipilih berdasarkan jenis kendaraan yang cocok, dan pengguna dapat memilih pengemudi yang diinginkan.

3. Pembayaran dan Kembalian: Setelah perjalanan selesai, metode `requestRide()` mengatur proses pembayaran dengan opsi pembayaran dan perhitungan kembalian. Pengguna diminta memilih metode pembayaran dan memasukkan jumlah pembayaran. Aplikasi melakukan validasi dan perhitungan matematika untuk memastikan pembayaran yang benar.


## Mampu menjelaskan algoritma dari solusi yang dibuat

Tentu! Berikut adalah penjelasan mengenai algoritma dari solusi yang dibuat:

1. Langkah Persiapan:
   - Membuat kelas `User`, `Driver`, dan `Ride` sebagai representasi objek dalam sistem Ojek Online.
   - Membuat kelas `OjekOnlineApp` sebagai kelas utama yang mengelola pengguna, pengemudi, dan perjalanan.
   - Membuat metode dan atribut yang diperlukan dalam setiap kelas.

2. Registrasi Pengguna:
   - Pengguna diminta memasukkan nama dan nomor telepon saat registrasi.
   - Objek `User` baru dibuat dengan informasi yang diberikan.
   - Objek `User` ditambahkan ke dalam daftar pengguna di `OjekOnlineApp`.

3. Proses Login:
   - Pengguna diminta memasukkan nama dan nomor telepon saat login.
   - Informasi yang dimasukkan dibandingkan dengan informasi pengguna yang terdaftar.
   - Jika informasi cocok, pengguna dianggap berhasil login dan dapat melanjutkan ke langkah selanjutnya.

4. Menambahkan Pengemudi:
   - Objek `Driver` dibuat dengan informasi pengemudi yang diberikan.
   - Objek `Driver` ditambahkan ke dalam daftar pengemudi di `OjekOnlineApp`.

5. Permintaan Perjalanan:
   - Pengguna diminta memasukkan lokasi penjemputan, tujuan, jarak, dan jenis kendaraan yang diinginkan.
   - `OjekOnlineApp` memeriksa daftar pengemudi yang tersedia dengan jenis kendaraan yang sesuai.
   - Jika ada pengemudi yang tersedia, pengemudi tersebut ditampilkan kepada pengguna.
   - Pengguna memilih pengemudi yang diinginkan dengan memilih nomor yang sesuai.
   - Objek `Ride` dibuat dengan informasi pengguna, pengemudi, lokasi penjemputan, tujuan, dan jarak.
   - Objek `Ride` ditambahkan ke dalam daftar perjalanan di `OjekOnlineApp`.
   - Pengemudi yang dipilih dihapus dari daftar pengemudi.

6. Pembayaran:
   - Pengguna diminta memilih metode pembayaran (DANA atau Tunai).
   - Jumlah pembayaran dimasukkan oleh pengguna.
   - Jumlah pembayaran dibandingkan dengan tarif perjalanan.
   - Jika pembayaran sesuai, proses pembayaran selesai.
   - Jika pembayaran lebih dari tarif perjalanan, kembalian dihitung dan ditampilkan kepada pengguna.
   - Jika pembayaran kurang dari tarif perjalanan, pesan kesalahan ditampilkan dan pengguna diminta memasukkan jumlah pembayaran yang benar.

7. Menampilkan Detail Perjalanan:
   - `OjekOnlineApp` mencetak detail perjalanan, termasuk informasi pengguna, pengemudi, lokasi penjemputan, tujuan, jarak, dan tarif perjalanan.


## Mampu menjelaskan konsep dasar OOP

1. Objek: Objek adalah representasi konkret dari konsep atau entitas di dunia nyata. Dalam OOP, objek memiliki atribut (data) dan metode (fungsi) yang berhubungan dengan objek tersebut. Misalnya, dalam konteks Ojek Online, objek-objek seperti `User`, `Driver`, dan `Ride` mewakili pengguna, pengemudi, dan perjalanan.

2. Class: Kelas adalah blueprint atau template yang mendefinisikan struktur, atribut, dan perilaku objek. Kelas berfungsi sebagai entitas yang mendefinisikan objek dan menyediakan cara untuk membuat objek dari kelas tersebut. Misalnya, kelas `User` dan `Driver` digunakan untuk membuat objek pengguna dan pengemudi.

3. Enkapsulasi: Enkapsulasi adalah konsep yang menggabungkan data (atribut) dan operasi (metode) yang berkaitan dengan objek ke dalam satu kesatuan. Ini berarti data dan metode yang relevan dibungkus bersama dalam kelas, sehingga dapat diakses dan digunakan secara terkendali. Dalam OOP, enkapsulasi digunakan untuk mengatur akses ke data dan metode dengan menggunakan akses modifier seperti `public`, `private`, atau `protected`.

4. Pewarisan (Inheritance): Pewarisan adalah konsep yang memungkinkan sebuah kelas mewarisi atribut dan metode dari kelas lain yang disebut kelas induk atau superclass. Kelas yang mewarisi atribut dan metode disebut kelas anak atau subclass. Pewarisan memungkinkan untuk membangun hierarki kelas dan menciptakan hierarki konsep yang lebih umum ke yang lebih spesifik. Ini juga memungkinkan untuk membagikan dan mengelompokkan kode yang berulang dan meningkatkan kode yang dapat digunakan kembali.

5. Polimorfisme (Polymorphism): Polimorfisme adalah kemampuan untuk menggunakan satu entitas (misalnya, metode atau objek) dengan berbagai cara. Dalam OOP, polimorfisme terjadi ketika objek dari kelas yang berbeda dapat diakses menggunakan referensi kelas yang sama, dan metode yang sama dapat memberikan perilaku yang berbeda tergantung pada objek yang digunakan. Polimorfisme memungkinkan fleksibilitas dan penggunaan kode yang lebih umum.

6. Abstraksi: Abstraksi adalah konsep untuk menyembunyikan detail implementasi dan hanya mengekspos informasi yang relevan atau penting. Dalam OOP, abstraksi digunakan untuk membuat kelas abstrak yang berfungsi sebagai kerangka kerja umum yang tidak bisa diinstansiasi secara langsung. Kelas abstrak mendefinisikan metode abstrak yang harus diimplementasikan oleh kelas anak. Abstraksi memungkinkan untuk memfokuskan pada fitur dan perilaku penting suatu objek tanpa terlalu terikat pada detail implementasinya.


## Mampu mendemonstrasikan penggunaan Encapsulation secara tepat

Contoh pada class User
    class User {
    private String name;
    private String phoneNumber;

    public User(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String toString() {
        return "User: " + name + ", Phone: " + phoneNumber;
    }
    }
Pada kelas User, atribut name dan phoneNumber dideklarasikan sebagai private, sehingga hanya dapat diakses melalui metode-metode publik seperti getName() dan getPhoneNumber(). Dengan demikian, akses langsung ke atribut dilindungi dan kontrol diberikan kepada metode-metode tersebut untuk membaca data atau mengembalikan nilainya.

## Mampu mendemonstrasikan penggunaan Abstraction secara tepat

    abstract class Ride {
    // ...
    public abstract double calculateFare();
    // ...
    }
Kelas Ride diubah menjadi kelas abstrak dengan menambahkan kata kunci abstract di depan definisi kelas. Selain itu, metode calculateFare() juga dideklarasikan sebagai metode abstrak dengan tidak memberikan implementasi. Dengan demikian, kelas Ride tidak dapat diinstansiasi secara langsung, tetapi hanya bisa menjadi kerangka kerja umum untuk kelas-kelas turunannya yang harus mengimplementasikan metode abstrak ini.

## Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat
Contoh :
    
    class Driver {
        // ...
    }

    class MotorDriver extends Driver {
        // ...
    }

    class CarDriver extends Driver {
        // ...
    }
Dalam contoh di atas, kelas MotorDriver dan CarDriver merupakan kelas turunan dari kelas Driver. Mereka mewarisi atribut dan metode yang didefinisikan dalam kelas Driver. Dengan pewarisan, kelas turunan dapat memperluas fungsionalitas kelas induk dan menambahkan perilaku yang spesifik.

## Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis ke dalam OOP?
1. Mengidentifikasi entitas atau objek yang sekiranya terlibat pada proses bisnis tsb.
2. Menentukan atribut dan entitasnya
3. Membuat relasi antar kelasnya
4. Mengimplementasikan ke dalam pemrogram
5. Pengujian

## Mampu menjelaskan rancangan dalam bentuk  Use Case table

- Usecase user

| Usecase | Nilai |
| ------ | ------ |
|   Pendaftaran Pengguna Baru |    90    |
|    Login dan Autentikasi |    95    |
|     Penjadwalan Layanan |80        |
| Pemesanan Layanan Transportasi| 93        |
|      Pemesanan Makanan   |      80  |
|       Pembayaran dan Penagihan|      90|
|      Penilaian dan Ulasan   | 70        |
|     Pelacakan Layanan  |85       |
|      Promosi dan Diskon  |    75    |

- Usecase Manajemen Perusahaan

| Usecase | Nilai |
| ------ | ------ |
|      Manajemen Pengemudi  |  90      |
|     Manajemen Layanan   |     85   |
|    Manajemen Pelanggan    |  90      |
|   Manajemen Keuangan     |  80      |


- Usecase Direksi Perusahaan

| Usecase | Nilai |
| ------ | ------ |
|      Perencanaan  |     95   |
|    Pengawasan Kinerja    |    90    |
| Pengembangan & Pemeliharaan|   85     |
|      Keuangan & Investasi  |       90 |

## YT
https://youtu.be/FmSONzUBHRw


